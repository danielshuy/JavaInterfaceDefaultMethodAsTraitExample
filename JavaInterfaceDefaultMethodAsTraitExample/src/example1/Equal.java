package example1;

/**
 *
 * @author Daniel
 * @param <E>
 */

/**
 * A simple example of how we can reuse code with {@code default} methods
 * @author Daniel
 * @param <E> 
 */
public interface Equal<E> {
    public abstract boolean isEqual(E e);
    
    /**
     * With this concrete method, this method no longer needs to be Overridden in each implementation of this {@code interface}
     * @param e
     * @return 
     */
    default boolean isNotEqual(E e) {
        return !this.isEqual(e);
    }
}
