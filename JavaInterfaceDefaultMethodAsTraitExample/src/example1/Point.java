package example1;

/**
 * 
 * @author Daniel
 */
public class Point implements Equal<Point> {
    private final int x;
    private final int y;
    
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public int getX() {
        return this.x;
    }
    
    public int getY() {
        return this.y;
    }
    
    @Override
    public boolean isEqual(Point pt) {
        return (this.x == pt.getX() && this.y == pt.getY());
    }
    
    public static void main(String[] args) {
        Point pt1 = new Point(0, 0);
        Point pt2 = new Point(1, 0);
        Point pt3 = new Point(1, 0);
        
        System.out.println(pt1.isEqual(pt2));
        System.out.println(pt1.isNotEqual(pt2));
        System.out.println(pt1.isEqual(pt3));
        System.out.println(pt1.isNotEqual(pt3));
        System.out.println(pt2.isEqual(pt3));
        System.out.println(pt2.isNotEqual(pt3));
    }
}
