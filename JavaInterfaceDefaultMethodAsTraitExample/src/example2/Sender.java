package example2;

/**
 * A simple example of Overloading {@code abstract} methods using {@code default} methods
 * @author Daniel
 */
public interface Sender {
   public abstract void send(String message);
   
   default void send() {
       this.send(null);
   }
}
