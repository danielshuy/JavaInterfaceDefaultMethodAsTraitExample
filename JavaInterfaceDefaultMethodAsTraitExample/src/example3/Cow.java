package example3;

/**
 *
 * @author Daniel
 */
public class Cow extends Animal implements HasTail {
    @Override
    public void speak() {
        System.out.println(Cow.class.getSimpleName() + ": \"Moo!\"");
    }
    
    public void swatFly() {
        this.wagTail();
    }
}
