package example3;

/**
 *
 * @author Daniel
 */
public class Bat extends Animal {
    @Override
    public void speak() {
        System.out.println(Bat.class.getSimpleName() + ": \"Scree!\"");
    }
}
