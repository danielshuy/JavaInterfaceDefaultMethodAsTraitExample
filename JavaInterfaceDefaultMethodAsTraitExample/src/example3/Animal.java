package example3;

/**
 * In this example, the {@link Pet} and {@link HasTail} interfaces have concrete {@code default} methods, allowing them to behave like Traits.
 * <p>This allows them to be implemented alone (eg. {@link Cow}/{@link Bat}), or together (eg. {@link Dog})
 * <p>Without Traits, more interfaces would be required to achieve the same thing (eg. AnimalWithTail)
 * @author Daniel
 */
public abstract class Animal {
    public abstract void speak();
    
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.ownerIsHome();
        
        Frog frog = new Frog();
        frog.ownerIsHome();
        
        Cow cow = new Cow();
        cow.swatFly();
        
        Bat bat = new Bat();
        
        Animal[] animals = new Animal[]{dog, frog, cow, bat};
        for (Animal animal: animals) {
            animal.speak();
        }
    }
}
