package example3;

/**
 *
 * @author Daniel
 */
public interface HasTail {
    default void wagTail() {
        System.out.println(this.getClass().getSimpleName() + ": Wags Tail");
    }
}
