package example3;

/**
 *
 * @author Daniel
 */
public class Dog extends Pet implements HasTail {
    @Override
    public void speak() {
        System.out.println(Dog.class.getSimpleName() + ": \"WOOF!\"");
    }

    @Override
    public void ownerIsHome() {
        this.wagTail();
        super.ownerIsHome();
    }
}
