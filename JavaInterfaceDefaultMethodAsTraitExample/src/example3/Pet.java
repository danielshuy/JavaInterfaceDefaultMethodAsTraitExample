package example3;

/**
 *
 * @author Daniel
 */
public abstract class Pet extends Animal {
    public void ownerIsHome() {
        this.speak();
    }
}
