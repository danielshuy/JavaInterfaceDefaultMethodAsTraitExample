package example3;

/**
 *
 * @author Daniel
 */
public class Frog extends Pet {
    @Override
    public void speak() {
        System.out.println(Frog.class.getSimpleName() + ": \"Croak!\"");
    }
}
